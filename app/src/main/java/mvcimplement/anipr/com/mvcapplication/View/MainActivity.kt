package mvcimplement.anipr.com.mvcapplication.View

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.list_item.view.*
import mvcimplement.anipr.com.apputils.ToastUtil
import mvcimplement.anipr.com.mvcapplication.DaggerComponent.DaggerMyComponent
import mvcimplement.anipr.com.mvcapplication.DaggerComponent.MyComponent
import mvcimplement.anipr.com.mvcapplication.Model.*
import mvcimplement.anipr.com.mvcapplication.Presenter.MainListPresenter
import mvcimplement.anipr.com.mvcapplication.Provider.ContextProvider
import mvcimplement.anipr.com.mvcapplication.R
import javax.inject.Inject


class MainActivity : AppCompatActivity(), MainListPresenter.MainListActions {

    var vehicle: Vehicle? = null

    @Inject
    lateinit var contextProvider: ContextProvider

    lateinit var prsenter: MainListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        val injected = if (contextProvider == null) false else true
//        demoText.setText("Dependency injection worked: " + ""+injected);

        //Initialize presenter
        prsenter = MainListPresenter(this)


        var componenet: VehicleComponent = DaggerVehicleComponent.builder().
                vehicleModule(VehicleModule()).contextProvider(ContextProvider(this)).build()

        var componenet1: MyComponent = DaggerMyComponent.builder().
                contextProvider(ContextProvider(this)).build()

        vehicle = componenet.provideVehicle()

        var currentContent = componenet1.providesContext()
        val injected = currentContent != null

//        noItemText.setText("Dependency injection worked: " + "" + injected + currentContent.applicationInfo.className);
        Log.d("Speed", "" + (vehicle!!.speed))

        setUpRecycleView()
        prsenter.callAPI(this@MainActivity)

    }


    /**
     * SetUp recycle view before setting adapter
     */
    fun setUpRecycleView() {
        rvItemsList.layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun showListView() {
        rvItemsList.visibility = View.VISIBLE
    }

    override fun hideListView() {
        rvItemsList.visibility = View.GONE
    }

    override fun showNoItem() {
        noItemText.visibility = View.VISIBLE
    }

    override fun hideNoItem() {
        noItemText.visibility = View.GONE
    }

    override fun closeListView() {
        finish()
    }


    override fun showFailMessage(message: String?) {
        ToastUtil.DisplayToast(this@MainActivity, "Server error found.")
    }

    override fun loadListItems(gitUsers: MutableList<GitUser>?) {
        rvItemsList.adapter = MyAdapter(gitUsers)
        hideNoItem()
        showListView()
    }


    class MyAdapter() : RecyclerView.Adapter<ItemHolder>() {
        var gitUsers: MutableList<GitUser>? = null

        constructor(gitUsers: MutableList<GitUser>?) : this() {
            this.gitUsers = gitUsers
        }

        override fun onBindViewHolder(holder: ItemHolder?, position: Int) {
            holder!!.bindItems(gitUsers!!.get(position))
        }

        override fun getItemCount(): Int {
            return gitUsers!!.size
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ItemHolder {
            val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.list_item, parent, false)
            return ItemHolder(itemView)
        }

    }

    class ItemHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(gitUser: GitUser) {
            itemView.name.text = gitUser.name
            itemView.user_id.text = gitUser.id
            itemView.full_name.text = gitUser.full_name

            Glide.with(itemView.context).load(gitUser.avatar_url).into(itemView.userImage)

        }

    }

}
