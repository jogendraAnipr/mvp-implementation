package mvcimplement.anipr.com.mvcapplication.View

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_login.*
import mvcimplement.anipr.com.apputils.ToastUtil
import mvcimplement.anipr.com.mvcapplication.Presenter.LoginPresenter
import mvcimplement.anipr.com.mvcapplication.R


class LoginActivity : AppCompatActivity(), LoginPresenter.LoginActions, View.OnClickListener {

    var n = 0
    lateinit var presenter:LoginPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //Initialize presenter
        presenter=LoginPresenter(this)

        //Click listener
        btnLogin.setOnClickListener(this)
        btnSignUp.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnLogin -> {
                hideLoginButton()
                showLoading()
//                Thread.sleep(1000)
                showSuccessMessage()
                showMainActivity()
                finish()
            }
            R.id.btnSignUp -> {
                showSignUpActivity()
            }
        }
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun showLoginButton() {
        btnLogin.visibility = View.VISIBLE
    }

    override fun hideLoginButton() {
        btnLogin.visibility = View.INVISIBLE

    }

    override fun loginAPiCall() {
    }

    override fun showMainActivity() {
        val mainActivity = Intent(this@LoginActivity, MainActivity::class.java)
        startActivity(mainActivity)
    }

    override fun showSignUpActivity() {
        val mainActivity = Intent(this@LoginActivity, SignUpActivity::class.java)
        startActivity(mainActivity)
    }

    override fun showSuccessMessage() {
        ToastUtil.DisplayToast(this@LoginActivity, "Login successful!")
    }

    override fun showFailMessage() {
        ToastUtil.DisplayToast(this@LoginActivity, "Login failed!")
    }

}
