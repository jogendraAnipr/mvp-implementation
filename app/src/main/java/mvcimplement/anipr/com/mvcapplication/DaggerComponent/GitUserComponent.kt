package mvcimplement.anipr.com.mvcapplication.DaggerComponent

import dagger.Component
import mvcimplement.anipr.com.mvcapplication.Model.GitUser
import javax.inject.Singleton

/**
 * Created by jogendra on 1/16/2018.
 * Component class provides dependency classes
 */
//
//@Singleton
//@Component(modules = [GitUser::class])
interface GitUserComponent {
//    fun providesGitUser(): GitUser
}
