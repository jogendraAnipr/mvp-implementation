package mvcimplement.anipr.com.mvcapplication

import android.app.Application
import android.content.Context
import android.text.TextUtils
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

/**
 * Created by jogendra on 1/16/2018.
 * Application class
 */
class MvpApplication : Application() {
    val TAG = MvpApplication::class.java!!
            .getSimpleName()

    private var mInstance: MvpApplication? = null
    private var mRequestQueue: RequestQueue? = null

    override fun onCreate() {
        super.onCreate()

    }


//    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
//        return DaggerAppComponent.builder().application(this).build()
//    }

    fun getRequestQueue(context: Context): RequestQueue? {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context)
        }

        return mRequestQueue
    }

    @Synchronized
    fun getInstance(): MvpApplication? {
        return mInstance
    }
//
//    fun <T> addToRequestQueue(req: Request<T>, tag: String) {
//        // set the default tag if tag is empty
//        req.tag = if (TextUtils.isEmpty(tag)) TAG else tag
//        getRequestQueue()!!.add(req)
//    }
//
//    fun <T> addToRequestQueue(req: Request<T>) {
//        req.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 20, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
//        req.tag = TAG
//        getRequestQueue()!!.add(req)
//    }
//
//    fun cancelPendingRequests(tag: Any) {
//        if (mRequestQueue != null) {
//            mRequestQueue!!.cancelAll(tag)
//        }
//    }

}