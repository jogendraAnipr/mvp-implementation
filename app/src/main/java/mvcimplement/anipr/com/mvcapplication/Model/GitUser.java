package mvcimplement.anipr.com.mvcapplication.Model;

import org.json.JSONObject;

import dagger.Module;

/**
 * Created by jogendra on 1/17/2018.
 * Model class holding detail of Git User
 */
 public class GitUser {
    String id, name, full_name, avatar_url;

    public GitUser() {
    }



    public GitUser(JSONObject jsonObject) {
        this.id = jsonObject.optString("id");
        this.name = jsonObject.optString("name");
        this.full_name = jsonObject.optString("full_name");
        if (jsonObject.optJSONObject("owner") != null)
            this.avatar_url = jsonObject.optJSONObject("owner").optString("avatar_url");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }
}

