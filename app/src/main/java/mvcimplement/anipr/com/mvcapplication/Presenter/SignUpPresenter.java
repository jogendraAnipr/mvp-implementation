package mvcimplement.anipr.com.mvcapplication.Presenter;

import mvcimplement.anipr.com.mvcapplication.ViewActions;
import mvcimplement.anipr.com.mvcapplication.ViewPresenter;

/**
 * Created by jogendra on 1/16/2018.
 * Presenter class for SignUp View
 */

public class SignUpPresenter extends ViewPresenter<SignUpPresenter.SignUpActions> {

    public SignUpPresenter(SignUpPresenter.SignUpActions view) {
        super.mView = view;
        super.mPresenter = this;
    }

    boolean validateFields(String userName, String password) {
        return true;
    }


    /**
     * User Actions required in SignUp View
     */
    public interface SignUpActions extends ViewActions {

        void showLoading();

        void hideLoading();

        void showSigUpButton();

        void hideSignUpButton();

        void  SignUpAPICall();

        void closeActivity();

        void showSuccessMessage(String message);

        void showFailMessage(String message);

        void startMainActivity();
    }
}
