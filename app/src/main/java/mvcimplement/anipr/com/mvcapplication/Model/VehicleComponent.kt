package mvcimplement.anipr.com.mvcapplication.Model

import dagger.Component
import mvcimplement.anipr.com.mvcapplication.Provider.ContextProvider
import javax.inject.Singleton


/**
 * Created by jogendra on 1/16/2018.
 */
@Singleton
@Component(modules = [VehicleModule::class, ContextProvider::class])
interface VehicleComponent {

    fun provideVehicle(): Vehicle

    fun provideContextProvider(): ContextProvider

}