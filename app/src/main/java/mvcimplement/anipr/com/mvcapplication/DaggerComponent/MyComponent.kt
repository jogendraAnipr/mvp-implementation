package mvcimplement.anipr.com.mvcapplication.DaggerComponent

import android.content.Context
import dagger.Component
import mvcimplement.anipr.com.mvcapplication.Provider.ContextProvider
import javax.inject.Singleton

/**
 * Created by jogendra on 1/16/2018.
 * Component class provides dependency classes
 */

@Singleton
@Component(modules = [ContextProvider::class])
interface MyComponent {

    fun providesContext(): Context

}
