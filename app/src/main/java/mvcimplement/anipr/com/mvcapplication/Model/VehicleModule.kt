package mvcimplement.anipr.com.mvcapplication.Model

import dagger.Module
import javax.inject.Singleton
import dagger.Provides



/**
 * Created by jogendra on 1/16/2018.
 */
@Module
class VehicleModule {

    @Provides
    @Singleton
    internal fun provideMotor(): Motor {
        return Motor()
    }

    @Provides
    @Singleton
    internal fun provideVehicle(): Vehicle {
        return Vehicle(Motor())
    }
}