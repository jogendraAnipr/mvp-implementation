package mvcimplement.anipr.com.mvcapplication.Provider;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jogendra on 1/16/2018.
 * Provider class to provide context for application
 */

@Module
public class ContextProvider {

    private Context context;

    @Inject
    public ContextProvider(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Context providesContext() {
        return context;
    }

}
