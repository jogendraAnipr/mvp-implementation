package mvcimplement.anipr.com.mvcapplication.Model

/**
 * Created by jogendra on 1/16/2018.
 */
class Motor {

    var rpm: Int = 0
        private set

    init {
        this.rpm = 0
    }

    fun accelerate(value: Int) {
        rpm = rpm + value
    }

    fun brake() {
        rpm = 0
    }
}