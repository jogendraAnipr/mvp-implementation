package mvcimplement.anipr.com.mvcapplication.Presenter;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mvcimplement.anipr.com.mvcapplication.Model.GitUser;
import mvcimplement.anipr.com.mvcapplication.MvpApplication;
import mvcimplement.anipr.com.mvcapplication.ViewActions;
import mvcimplement.anipr.com.mvcapplication.ViewPresenter;

/**
 * Created by jogendra on 1/16/2018.
 * Presenter class for Main List view
 */

public class MainListPresenter extends ViewPresenter<MainListPresenter.MainListActions> {

    public MainListPresenter(MainListPresenter.MainListActions mainListActions) {
        super.mView = mainListActions;
        super.mPresenter = this;
    }

    boolean validateFields(String userName, String password) {
        return true;
    }

    public void callAPI(Context context) {
        mView.showLoading();
        mView.hideNoItem();
        String url = "https://api.github.com/search/repositories?q=user";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Response", response.toString());
                JSONArray items = response.optJSONArray("items");
//                GitUserComponent gitUserComponent = DaggerGitUserComponent.builder().
//                        gitUser(new GitUser()).build();

                List<GitUser> gitUsers = new ArrayList<>();
                for (int i = 0; i < items.length(); i++) {
                    gitUsers.add(new GitUser(items.optJSONObject(i)));
                }

                mView.loadListItems(gitUsers);
                mView.hideLoading();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        new MvpApplication().getRequestQueue(context).add(jsonObjectRequest);

    }


    /**
     * User Actions required in MainList
     */
    public interface MainListActions extends ViewActions {

        void showLoading();

        void hideLoading();

        void showListView();

        void hideListView();

        void showNoItem();

        void hideNoItem();

        void closeListView();

        void loadListItems(List<GitUser> gitUsers);

        void showFailMessage(String message);
    }
}
