package mvcimplement.anipr.com.mvcapplication.Model

import javax.inject.Inject

/**
 * Created by jogendra on 1/16/2018.
 */
class Vehicle() {
    private var motor: Motor? = null

    @Inject
    constructor(motor: Motor) : this() {
        this.motor = motor
    }

    val speed: Int
        get() = motor!!.rpm

    fun increaseSpeed(value: Int) {
        motor!!.accelerate(value)
    }

    fun stop() {
        motor!!.brake()
    }
}