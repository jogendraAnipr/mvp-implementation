package mvcimplement.anipr.com.mvcapplication.Presenter;

import mvcimplement.anipr.com.mvcapplication.ViewActions;
import mvcimplement.anipr.com.mvcapplication.ViewPresenter;

/**
 * Created by jogendra on 1/16/2018.
 * Presenter class for Login View
 */

public class LoginPresenter extends ViewPresenter<LoginPresenter.LoginActions> {

    public LoginPresenter(LoginPresenter.LoginActions view) {
        super.mView = view;
        super.mPresenter = this;
    }

    boolean validateCredentials(String userName, String password) {
        return true;
    }


    /**
     * User Actions required in Login View
     */
    public interface LoginActions extends ViewActions {

        void showLoading();

        void hideLoading();

        void showLoginButton();

        void hideLoginButton();

        void loginAPiCall();

        void showMainActivity();

        void showSignUpActivity();

        void showSuccessMessage();

        void showFailMessage();
    }
}
