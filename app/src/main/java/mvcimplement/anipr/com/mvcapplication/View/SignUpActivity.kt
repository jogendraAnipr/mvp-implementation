package mvcimplement.anipr.com.mvcapplication.View

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_login.*
import mvcimplement.anipr.com.apputils.ToastUtil
import mvcimplement.anipr.com.mvcapplication.Presenter.SignUpPresenter
import mvcimplement.anipr.com.mvcapplication.R


class SignUpActivity : AppCompatActivity(), SignUpPresenter.SignUpActions, View.OnClickListener {

    var n = 0
    lateinit var presenter: SignUpPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        //Initialize presenter
        presenter = SignUpPresenter(this)
        btnSignUp.setOnClickListener(this)

        setSupportActionBar(toolBar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolBar.setNavigationOnClickListener { view ->
            finish()
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnSignUp -> {
                startMainActivity()

            }
        }
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun showSigUpButton() {
        btnSignUp.visibility = View.VISIBLE
    }

    override fun hideSignUpButton() {
        btnSignUp.visibility = View.GONE
    }

    override fun SignUpAPICall() {

    }

    override fun closeActivity() {
        finish()
    }

    override fun showSuccessMessage(message: String?) {
        ToastUtil.DisplayToast(this@SignUpActivity, "Success")
    }

    override fun showFailMessage(message: String?) {
        ToastUtil.DisplayToast(this@SignUpActivity, "fail")
    }

    override fun startMainActivity() {
        val mainIntent = Intent(this@SignUpActivity, MainActivity::class.java);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(mainIntent)
        finish()
    }
}
