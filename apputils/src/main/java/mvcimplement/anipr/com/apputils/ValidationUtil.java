package mvcimplement.anipr.com.apputils;

import android.text.TextUtils;

/**
 * Created by jogendra on 9/21/2016.
 * Utility class holds validation methods
 */
public class ValidationUtil {

    /**
     * Method to check valid eMail or not
     *
     * @param email emailId to check valid or not
     */
    public static boolean isValidEmail(String email) {
        return email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }

    /**
     * Method to check string empty or not
     */
    public static boolean isEmpty(String value) {
        if (value == null)
            return true;
        else {
            value = value.trim();
            return TextUtils.isEmpty(value);
        }
    }

    /**
     * checking same password or not
     *
     * @param newpassword     string to match password
     * @param confirmpassword string to macth password
     * @return true if password is same otherwise fasle
     */
    public static boolean isPasswordSame(String newpassword, String confirmpassword) {
        return newpassword.equals(confirmpassword);
    }
}
