package mvcimplement.anipr.com.apputils;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by satyam on 8/3/2017.
 * util class for parsing date
 */

public class DateParseUtil {
    public static String getDate(String strdate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        try {
            Date date = sdf.parse(strdate);
            sdf.applyPattern("dd MMM, yyyy");
            String date1 = sdf.format(date);
            Log.e("getDate: ", date1);
            return date1;
        } catch (Exception ex) { // here forgot the exact exception class Parse exception was used
            // do something here
            Log.e("error:", ex.toString());
            return strdate;
        }
    }
    public static String getAnotherFormatDate(String strdate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try {
            Date date = sdf.parse(strdate);
            sdf.applyPattern("dd MMM, yyyy");
            String date1 = sdf.format(date);
            Log.e("getDate: ", date1);
            return date1;
        } catch (Exception ex) { // here forgot the exact exception class Parse exception was used
            // do something here
            Log.e("error:", ex.toString());
            return strdate;
        }
    }

    public static String getReceiptDateFormat(String strdate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        try {
            Date date = sdf.parse(strdate);
            sdf.applyPattern("EEEE dd MMM, yyyy hh.mm a");
            String date1 = sdf.format(date);
            Log.e("getDate: ", date1);
            return date1;
        } catch (Exception ex) { // here forgot the exact exception class Parse exception was used
            // do something here
            Log.e("error:", ex.toString());
            return strdate;
        }
    }
    public static String getDateTimeFormat(String strdate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        try {
            Date date = sdf.parse(strdate);
            sdf.applyPattern("dd MMM, yyyy hh.mm a");
            String date1 = sdf.format(date);
            Log.e("getDate: ", date1);
            return date1;
        } catch (Exception ex) { // here forgot the exact exception class Parse exception was used
            // do something here
            Log.e("error:", ex.toString());
            return strdate;
        }
    }
    public static String getDateFormat(String strdate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        try {
            Date date = sdf.parse(strdate);
            sdf.applyPattern("dd MMM, yyyy");
            String date1 = sdf.format(date);
            Log.e("getDate: ", date1);
            return date1;
        } catch (Exception ex) { // here forgot the exact exception class Parse exception was used
            // do something here
            Log.e("error:", ex.toString());
            return strdate;
        }
    }
}
