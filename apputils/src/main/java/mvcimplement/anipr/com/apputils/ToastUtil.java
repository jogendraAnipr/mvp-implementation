package mvcimplement.anipr.com.apputils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by satyam on 8/1/2017.
 * utility class for showing toast
 */

public class ToastUtil {
    public static void DisplayToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
