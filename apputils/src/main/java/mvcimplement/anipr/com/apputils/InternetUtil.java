package mvcimplement.anipr.com.apputils;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by jogendra on 9/20/2016.
 * Util class to to hold internet related methods
 */
public class InternetUtil {

    /**
     * Method to check device connected to internet or not
     *
     * @param context contect to check
     */
    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

}
