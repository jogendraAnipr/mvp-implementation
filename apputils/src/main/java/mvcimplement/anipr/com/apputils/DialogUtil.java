package mvcimplement.anipr.com.apputils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;


/**
 * Created by jogendra on 9/21/2016.
 * Utility class to create and manage a generic dialog which will be used to show information through out the application
 */
public class DialogUtil {

    private AlertDialog dialog;
    private Activity activity;
    DialogInterface dialogInterface;


    public DialogUtil(Activity activity) {
        this.activity = activity;
    }

    /**
     * SetUp Dialog to display message
     */
    public void setUpDialog(String message, Context context) {
        dialog = new AlertDialog.Builder(activity).create();
        dialog.setTitle(context.getResources().getString(R.string.app_name));
        dialog.setMessage(message);
    }

    /**
     * Set button to access call backs
     *
     * @param WhichButtn      will be any static value from  @{@link DialogInterface} to define behaviour
     * @param text            Button text
     * @param onClickListener will be @{@link DialogInterface.OnClickListener} for button click call back
     */
    public void setDialogButton(int WhichButtn, String text, DialogInterface.OnClickListener onClickListener) {
        dialog.setButton(WhichButtn, text, onClickListener);
    }

    /**
     * Method to show dialog
     *
     * @param message Dialog message
     */
    public void showDialog(String message, Context context) {
        dialog = new AlertDialog.Builder(activity).create();
        dialog.setTitle(context.getResources().getString(R.string.app_name));
        dialog.setMessage(message);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismissDialog();
            }
        });
        if (!dialog.isShowing())
            dialog.show();
    }

    /**
     * Method to show dialog
     */
    public void showDialog() {
        if (dialog != null && !dialog.isShowing())
            dialog.show();
    }

    /**
     * Method to dismiss dialog
     */
    public void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
